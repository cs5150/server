package edu.cornell.svms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.cornell.svms.model.onload.Course;
import edu.cornell.svms.model.onload.HomeworkParent;
import edu.cornell.svms.model.onload.OnLoadStudent;
import edu.cornell.svms.model.onload.OnLoadTeacher;
import edu.cornell.svms.model.onload.Standard;
import edu.cornell.svms.model.onload.Student;
import edu.cornell.svms.model.onload.StudentID;
import edu.cornell.svms.model.onload.Teacher;
import edu.cornell.svms.repository.CourseRepository;
import edu.cornell.svms.repository.TeacherRepository;

@Controller
@RequestMapping(path = "/load")
@JsonInclude(Include.NON_NULL)
public class OnLoadController {
	
	public OnLoadController() {
		// TODO Auto-generated constructor stub
		onloadStudent();
		onloadTeacher();
	}
	
	void onloadStudent() {
		student = new OnLoadStudent();
//		student.setId(String.valueOf(id));
		Standard standard = new Standard();
		standard.setId(12);
		standard.setSection("A");
		standard.setStandard("6");
		student.setStandard(standard);
		ArrayList<String> subjectNames = new ArrayList<String>();
		subjectNames.add("English");
		subjectNames.add("Tamil");
		subjectNames.add("Science");
		subjectNames.add("Social Science");
		subjectNames.add("Maths");
		Map<String, Map<String, Integer>> marks= new HashMap<String, Map<String, Integer>>();
		HashMap<String, Integer> midterm1 = new HashMap<String, Integer>();
		HashMap<String, Integer> midterm2 = new HashMap<String, Integer>();
		HashMap<String, Integer> midterm3 = new HashMap<String, Integer>();
		HashMap<String, Integer> quart = new HashMap<String, Integer>();
		HashMap<String, Integer> half = new HashMap<String, Integer>();
		HashMap<String, Integer> annual = new HashMap<String, Integer>();
		
		midterm1.put("English", 40);
		midterm1.put("Tamil", 22);
		midterm1.put("Science", 11);
		midterm1.put("Social Science", 5);
		midterm1.put("Maths", 20);
		
		marks.put("MIDTERM 1", midterm1);
		
		
		midterm2.put("English", 47);
		midterm2.put("Tamil", 20);
		midterm2.put("Science", 17);
		midterm2.put("Social Science", 51);
		midterm2.put("Maths", 29);
		
		marks.put("MIDTERM 2", midterm2);
		
		midterm3.put("English", 30);
		midterm3.put("Tamil", 41);
		midterm3.put("Science", 77);
		midterm3.put("Social Science", 50);
		midterm3.put("Maths", 54);
		
		marks.put("MIDTERM 3", midterm3);
		
		quart.put("English", 89);
		quart.put("Tamil", 70);
		quart.put("Science", 60);
		quart.put("Social Science", 51);
		quart.put("Maths", 11);
		
		marks.put("QUARTERLY", quart);
		
		half.put("English", 40);
		half.put("Tamil", 22);
		half.put("Science", 11);
		half.put("Social Science", 5);
		half.put("Maths", 20);
		
		marks.put("HALF YEARLY", half);
		
		annual.put("English", 46);
		annual.put("Tamil", 29);
		annual.put("Science", 10);
		annual.put("Social Science", 57);
		annual.put("Maths", 26);
		
		marks.put("ANNUAL", annual);
		student.setMarks(marks);
		
		Map<String, ArrayList<String>> announcements= new HashMap<String, ArrayList<String>>();
		ArrayList<String> anno1 = new ArrayList<String>();
		ArrayList<String> anno2 = new ArrayList<String>();
		anno1.add("Saturday, 22nd April will be a holiday.");
		anno2.add("Come in sports uniform");
		
		announcements.put("22/03/2018", anno1);
		announcements.put("14/03/2018", anno2);
		
		Student s = new Student();
		s.setAddress("#12 Manjushree Nagar, Near TVS Nagar, Hosur");
		s.setClassTeacher("Grace Phebe");
		s.setContact("+91-9735373635");
//		s.setId(String.valueOf(id));
		s.setName("Sharan");
		s.setSchoolAddress("Anna Nagar, Belathur");
		s.setSchoolContact("+91-9789399670");
		student.setStudent(s);
		student.setSubjectNames(subjectNames);
		student.setAnnouncements(announcements);
	    Map<String, List<HomeworkParent>> homework= new HashMap<String, List<HomeworkParent>>();
	    student.setHomework(homework);
		
		HomeworkParent hw1= new HomeworkParent();
		hw1.setDescription("Complete worksheet on tenses.");
		hw1.setSubject("English");
		
		HomeworkParent hw2= new HomeworkParent();
		hw2.setDescription("Learn for test chapter 12, circles.");
		hw2.setSubject("Math");
		
		List<HomeworkParent> h1 = new ArrayList<HomeworkParent>();
		h1.add(hw1);
		h1.add(hw2);
		homework.put("10/03/2018",h1);
	}
	
	void onloadTeacher() {
		teacher = new OnLoadTeacher();
		Map<String, List<HomeworkParent>> homeworkin= new HashMap<String, List<HomeworkParent>>();
		Map<String, Map<String, List<HomeworkParent>>> homework = new HashMap<String, Map<String, List<HomeworkParent>>>();
		homework.put("English,2A", new HashMap<String, List<HomeworkParent>>());
		homework.put("Science,2B", new HashMap<String, List<HomeworkParent>>());
		homework.put("Science,3A", new HashMap<String, List<HomeworkParent>>());
		Course c = new Course();
			Standard standard = new Standard();
			standard.setId(12);
			standard.setSection("A");
			standard.setStandard("6");
			c.setId(3);
			c.setStandard(standard);
			c.setSubjectName("English");
			HomeworkParent hw1= new HomeworkParent();
			hw1.setDescription("Complete worksheet on tenses.");
//			hw1.setSubject("English");
			
			HomeworkParent hw2= new HomeworkParent();
			hw2.setDescription("Learn for test chapter 12, circles.");
//			hw2.setSubject("English");
			
			List<HomeworkParent> h1 = new ArrayList<HomeworkParent>();
			h1.add(hw1);
			List<HomeworkParent> h2 = new ArrayList<HomeworkParent>();
			h2.add(hw2);
			homeworkin.put("10/03/2018",h1);
			homeworkin.put("12/03/2018", h2);
			homework.put("English,6A", homeworkin);
			teacher.setHomework(homework);
			Map<String, ArrayList<String>> announcements= new HashMap<String, ArrayList<String>>();
			ArrayList<String> anno1 = new ArrayList<String>();
			ArrayList<String> anno2 = new ArrayList<String>();
			anno1.add("Saturday, 22nd April will be a holiday.");
			anno2.add("Come in sports uniform");
			
			announcements.put("22/03/2018", anno1);
			announcements.put("14/03/2018", anno2);
			
		
			teacher.setAnnouncements(announcements);
			
			Map<String, Map<String, Map<String, Integer>>> marks = 
					new HashMap<String, Map<String, Map<String,Integer>>>();
			
			Map<String, Integer> studentMarks = new HashMap<String, Integer>();
			studentMarks.put("Roshini,1", 54);
			studentMarks.put("Gokul,2", 83);   
			studentMarks.put("Sharan,3", 91);
			studentMarks.put("Kavya,4", 65);
			studentMarks.put("Rita,5", 53);
			studentMarks.put("Bharath,6", 77);
			studentMarks.put("Karthik,7", 75);
			studentMarks.put("Shruti,8", 68);
			
			
			Map<String, Integer> studentMarks1 = new HashMap<String, Integer>();
			studentMarks1.put("Roshini,1", 58);
			studentMarks1.put("Gokul,2", 93);   
			studentMarks1.put("Sharan,3", 81);
			studentMarks1.put("Kavya,4", 55);
			studentMarks1.put("Rita,5", 57);
			studentMarks1.put("Bharath,6", 87);
			studentMarks1.put("Karthik,7", 76);
			studentMarks1.put("Shruti,8", 62);
			
			
			
			Map<String, Integer> studentMarks2 = new HashMap<String, Integer>();
			studentMarks2.put("Roshini,1", 82);
			studentMarks2.put("Gokul,2", 94);   
			studentMarks2.put("Sharan,3", 77);
			studentMarks2.put("Kavya,4", 61);
			studentMarks2.put("Rita,5", 55);
			studentMarks2.put("Bharath,6", 79);
			studentMarks2.put("Karthik,7", 74);
			studentMarks2.put("Shruti,8", 72);
			
			Map<String, Integer> studentMarks3 = new HashMap<String, Integer>();
			studentMarks3.put("Roshini,1", 84);
			studentMarks3.put("Gokul,2", 91);   
			studentMarks3.put("Sharan,3", 70);
			studentMarks3.put("Kavya,4", 66);
			studentMarks3.put("Rita,5", 52);
			studentMarks3.put("Bharath,6", 74);
			studentMarks3.put("Karthik,7", 71);
			studentMarks3.put("Shruti,8", 68);
			
			Map<String, Integer> studentMarks4 = new HashMap<String, Integer>();
			studentMarks4.put("Roshini,1", 62);
			studentMarks4.put("Gokul,2", 90);   
			studentMarks4.put("Sharan,3", 80);
			studentMarks4.put("Kavya,4", 72);
			studentMarks4.put("Rita,5", 55);
			studentMarks4.put("Bharath,6", 79);
			studentMarks4.put("Karthik,7", 75);
			studentMarks4.put("Shruti,8", 86);
			
			Map<String, Map<String, Integer>> examT = new HashMap<String, Map<String, Integer>>();
			examT.put("MID TERM 1", studentMarks);
			examT.put("MID TERM 2", studentMarks1);
			examT.put("MID TERM 3", studentMarks2);
			examT.put("QUARTERLY", studentMarks3);
			examT.put("HALF YEARLY", studentMarks4);
//			examT.put("ANNUAL", studentMarks);
			
			marks.put("English,6A", examT);
			marks.put("English,2A", new HashMap<String, Map<String, Integer>>());
			marks.put("Science,2B", new HashMap<String, Map<String, Integer>>());
			marks.put("Science,3A", new HashMap<String, Map<String, Integer>>());
//			homework.put("English,2A", null);
//			homework.put("Science,2B", null);
//			homework.put("Science,3A", null);
			teacher.setMarks(marks);
			Teacher tea = new Teacher();
			tea.setAddress("#31, NGGO Colony, Hosur");
			tea.setClassTeacher(standard);
			tea.setContact("82873678325");
//			teacher.setId(String.valueOf(id));
			tea.setName("Grace Phebe");
			tea.setSchoolAddress("Anna Nagar, Belathur");
			tea.setSchoolContact("9736355633");
			teacher.setTeacher(tea);
	}
	

	@Autowired
	CourseRepository courseRepo;
	
	@Autowired
	TeacherRepository teacherRepo;
	
	OnLoadTeacher teacher;
	
	OnLoadStudent student;

	@RequestMapping(value = { "/student/{id}" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public OnLoadStudent getStudentOnLoad(@PathVariable("id") Long id) {
		student.setId(String.valueOf(id));
		return student;
	}
	
	@RequestMapping(value = { "/teacher/{id}" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public OnLoadTeacher getTeacherOnLoad(@PathVariable("id") Long id) {
		teacher.setId(String.valueOf(id));
		return teacher;
	}
	
	
	@RequestMapping(value = { "/teacher/{id}" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void postTeacherOnLoad(@PathVariable("id") Long id, @RequestBody OnLoadTeacher t) {
		teacher= t;
	}
	
	@RequestMapping(value = { "/student/{id}" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void postStudentOnLoad(@PathVariable("id") Long id, @RequestBody OnLoadStudent t) {
		student= t;
	}
	
	
}
