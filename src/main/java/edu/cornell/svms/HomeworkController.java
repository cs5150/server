package edu.cornell.svms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.cornell.svms.model.Course;
import edu.cornell.svms.model.Homework;
import edu.cornell.svms.model.Student;
import edu.cornell.svms.repository.CourseRepository;
import edu.cornell.svms.repository.HomeworkRepository;
import edu.cornell.svms.repository.StandardRepository;
import edu.cornell.svms.repository.StudentRepository;

@Controller
@RequestMapping(path = "/homework")
public class HomeworkController {

	@Autowired
	HomeworkRepository homeworkRepo;
	
	@Autowired
	StandardRepository standardRepo;
	
	@Autowired
	CourseRepository courseRepo;
	
	@Autowired
	StudentRepository studentRepo;

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Homework> getAllHomeworks() {
		List<Homework> allStudents = new ArrayList<Homework>();
		allStudents.addAll((List<Homework>) homeworkRepo.findAll());
		return allStudents;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public Homework getHomeworkById(@PathVariable("id") Long id) {
		Homework homework = homeworkRepo.findById(id);
		return homework;
	}

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Homework> addHomeworks(@RequestBody List<Homework> homework) {
		homeworkRepo.save(homework);
		return homework;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void deleteAllHomeworks(@PathVariable("id") Long id) {
		homeworkRepo.delete(id);
	}
	
	@RequestMapping(value = { "/standard/{standardid}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Homework> getHomeworksByStandard(@PathVariable("standardid") Long standardId) {
		List<Course> courses= courseRepo.findByStandardId(standardId);
		List<Homework> homeworks = new ArrayList<Homework>(); 
		for(Course course: courses) {
			homeworks.addAll(homeworkRepo.findByCourseId(course.getId()));
		}
		return homeworks;
	}
	
	@RequestMapping(value = { "/student/{studentid}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Homework> getHomeworksByStudent(@PathVariable("studentid") Long studentId) {
		Student student = studentRepo.findOne(studentId);
		List<Course> courses= courseRepo.findByStandardId(student.getStandardId());
		List<Homework> homeworks = new ArrayList<Homework>(); 
		for(Course course: courses) {
			homeworks.addAll(homeworkRepo.findByCourseId(course.getId()));
		}
		return homeworks;
	}
	
	@RequestMapping(value = { "/course/{courseid}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Homework> getHomeworksByCourse(@PathVariable("courseid") Long courseId) {
	return homeworkRepo.findByCourseId(courseId);
	}

}
