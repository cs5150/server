package edu.cornell.svms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.cornell.svms.model.Student;
import edu.cornell.svms.repository.StudentRepository;

@Controller
@RequestMapping(path = "/student")
public class StudentController {

	@Autowired
	StudentRepository studentRepo;

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Student> getAllStudents() {
		List<Student> allStudents = new ArrayList<Student>();
		allStudents.addAll((List<Student>) studentRepo.findAll());
		return allStudents;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public Student getStudentById(@PathVariable("id") Long id) {
		return studentRepo.findOne(id);
	}

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Student> postStudents(@RequestBody List<Student> students) {
		studentRepo.save(students);
		return students;
	}
	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllStudents(@RequestBody List<Student> students) {
//		studentRepo.delete(students);
//	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void deleteStudentById(@PathVariable("id") Long id) {
		studentRepo.delete(id);
	}

}
