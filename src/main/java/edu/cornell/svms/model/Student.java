package edu.cornell.svms.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.cornell.svms.enumeration.BusStop;

@Entity
@Table(name = "student")
@JsonInclude(Include.NON_NULL)
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "admission_no", unique = true)
	private String admissionNo;

	@Column(name = "name")
	private String name;

	@Column(name = "dob")
	private Date dob;

	@Column(name = "standard_id")
	private Long standardId;

	@Column(name = "primary_contact")
	private String primaryContact;

	@Column(name = "secondary_contact")
	private String secondaryContact;

	@Column(name = "address")
	private String address; // comma separated string of Door no, Street, Village and Pin code

	@Column(name = "bus_stop")
	private BusStop busStop;
	
	@ElementCollection
    @CollectionTable(name = "student_absent_dates", joinColumns = @JoinColumn(name = "student_id"))
    @Column(name = "absent_date")
    private Set<Long> absentDates = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdmissionNo() {
		return admissionNo;
	}

	public void setAdmissionNo(String admissionNo) {
		this.admissionNo = admissionNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BusStop getBusStop() {
		return busStop;
	}

	public void setBusStop(BusStop busStop) {
		this.busStop = busStop;
	}

	public Set<Long> getAbsentDates() {
		return absentDates;
	}

	public void setAbsentDates(Set<Long> absentDates) {
		this.absentDates = absentDates;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Student [id=");
		builder.append(id);
		builder.append(", admissionNo=");
		builder.append(admissionNo);
		builder.append(", name=");
		builder.append(name);
		builder.append(", dob=");
		builder.append(dob);
		builder.append(", standardId=");
		builder.append(standardId);
		builder.append(", primaryContact=");
		builder.append(primaryContact);
		builder.append(", secondaryContact=");
		builder.append(secondaryContact);
		builder.append(", address=");
		builder.append(address);
		builder.append(", busStop=");
		builder.append(busStop);
		builder.append(", absentDates=");
		builder.append(absentDates);
		builder.append("]");
		return builder.toString();
	}

}
