package edu.cornell.svms.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "standard")
@JsonInclude(Include.NON_NULL)
public class Standard {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name= "standard")
	private String standard; // LKG, UKG, 1,2,3...12

	@Column(name= "section")
	private String section;

	@ElementCollection
    @CollectionTable(name = "standard_course_ids", joinColumns = @JoinColumn(name = "standard_id"))
    @Column(name = "courses_id")
    private Set<Long> coursesIds = new HashSet<>(); 
	
	@Column(name= "ct_teacher_id")
	private Long ctTeacherId;
	
	@ElementCollection
    @CollectionTable(name = "standard_student_ids", joinColumns = @JoinColumn(name = "standard_id"))
    @Column(name = "student_id")
    private Set<Long> studentsIds = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Set<Long> getCoursesIds() {
		return coursesIds;
	}

	public void setCoursesIds(Set<Long> coursesIds) {
		this.coursesIds = coursesIds;
	}

	public Long getCtTeacherId() {
		return ctTeacherId;
	}

	public void setCtTeacherId(Long ctTeacherId) {
		this.ctTeacherId = ctTeacherId;
	}

	public Set<Long> getStudentsIds() {
		return studentsIds;
	}

	public void setStudentsIds(Set<Long> studentsIds) {
		this.studentsIds = studentsIds;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Standard [id=");
		builder.append(id);
		builder.append(", standard=");
		builder.append(standard);
		builder.append(", section=");
		builder.append(section);
		builder.append(", coursesIds=");
		builder.append(coursesIds);
		builder.append(", ctTeacherId=");
		builder.append(ctTeacherId);
		builder.append(", studentsIds=");
		builder.append(studentsIds);
		builder.append("]");
		return builder.toString();
	}

}
