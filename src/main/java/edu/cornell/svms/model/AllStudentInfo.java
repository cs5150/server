package edu.cornell.svms.model;

import java.util.ArrayList;
import java.util.List;

public class AllStudentInfo {

	private Student studentInfo;
    
    private Standard standard;
    
    private List<Homework> homework = new ArrayList<Homework>(); // Last 10 week 

	public Student getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(Student studentInfo) {
		this.studentInfo = studentInfo;
	}

	public Standard getStandard() {
		return standard;
	}

	public void setStandard(Standard standard) {
		this.standard = standard;
	}

	public List<Homework> getHomework() {
		return homework;
	}

	public void setHomework(List<Homework> homework) {
		this.homework = homework;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AllStudentInfo [studentInfo=");
		builder.append(studentInfo);
		builder.append(", standard=");
		builder.append(standard);
		builder.append(", homework=");
		builder.append(homework);
		builder.append("]");
		return builder.toString();
	}

	
    
}
