package edu.cornell.svms.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.cornell.svms.enumeration.BusStop;

@Entity
@Table(name = "teacher")
@JsonInclude(Include.NON_NULL)
public class Teacher {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "emp_no", unique = true)
	private String empNo;

	@Column(name = "name")
	private String name;

	@Column(name = "contact")
	private String contact;

	@Column(name = "address")
	private String address; // comma separated string of Door no, Street, Village and Pin code

	@Column(name = "bus_stop")
	private BusStop busStop;

	@Column(name = "dob")
	private Date dob;

//	
//	@ElementCollection
//    @CollectionTable(name = "teacher_courses", joinColumns = @JoinColumn(name = "teacher_id"))
//    @Column(name = "course_id")
//    private Set<Long> courseIds = new HashSet<>();

	@Column(name= "ct_class_id")
	private Long ctClassId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BusStop getBusStop() {
		return busStop;
	}

	public void setBusStop(BusStop busStop) {
		this.busStop = busStop;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

//	public Set<Long> getCourseIds() {
//		return courseIds;
//	}
//
//	public void setCourseIds(Set<Long> courseIds) {
//		this.courseIds = courseIds;
//	}

	public Long getCtClassId() {	
		return ctClassId;
	}

	public void setCtClassId(Long ctClassId) {
		this.ctClassId = ctClassId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Teacher [id=");
		builder.append(id);
		builder.append(", empNo=");
		builder.append(empNo);
		builder.append(", name=");
		builder.append(name);
		builder.append(", contact=");
		builder.append(contact);
		builder.append(", address=");
		builder.append(address);
		builder.append(", busStop=");
		builder.append(busStop);
		builder.append(", dob=");
		builder.append(dob);
//		builder.append(", courseIds=");
//		builder.append(courseIds);
		builder.append(", ctClassId=");
		builder.append(ctClassId);
		builder.append("]");
		return builder.toString();
	}
	// TODO: Define the relationship bw teacher, subject & class
}
