package edu.cornell.svms.model;

public enum ErrorMessage {
	INVALID_COURSE,
	INVALID_TEACHER,
	INVALID_STUDENT,
	INVALID_HW,
	SUCCESS;
}
