package edu.cornell.svms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.cornell.svms.enumeration.Subject;

@Entity
@Table(name = "course")
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "subject_name")
	private Subject subjectName;
	
	@Column(name= "standard_id")
	private Long standardId;
	
	
    @Column(name="teacher_id")
	private Long teacherId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Subject getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(Subject subjectName) {
		this.subjectName = subjectName;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Course [id=");
		builder.append(id);
		builder.append(", subjectName=");
		builder.append(subjectName);
		builder.append(", standardId=");
		builder.append(standardId);
		builder.append(", teacherId=");
		builder.append(teacherId);
		builder.append("]");
		return builder.toString();
	}
}
