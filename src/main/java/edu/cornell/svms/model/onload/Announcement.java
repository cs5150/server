package edu.cornell.svms.model.onload;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by apple on 3/24/18.
 */

public class Announcement implements Serializable {
    private Date date;
    private String description;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
