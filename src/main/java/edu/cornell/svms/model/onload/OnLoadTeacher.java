package edu.cornell.svms.model.onload;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 3/24/18.
 */


public class OnLoadTeacher implements Serializable {
    private String id;
    private Teacher teacher;
    private Map<String, Map<String, List<HomeworkParent>>> homework;
    private Map<String, Map<String, Map<String, Integer>>> marks;  // first key is subject type, second key is exam name, third key is the student name & id
    private Map<String, ArrayList<String>> announcements;

    public Teacher getTeacher() {
        return teacher;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


	public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

	public Map<String, ArrayList<String>> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(Map<String, ArrayList<String>> announcements) {
		this.announcements = announcements;
	}

	public Map<String, Map<String, List<HomeworkParent>>> getHomework() {
		return homework;
	}

	public void setHomework(Map<String, Map<String, List<HomeworkParent>>> homework) {
		this.homework = homework;
	}

	public Map<String, Map<String, Map<String, Integer>>> getMarks() {
		return marks;
	}

	public void setMarks(Map<String, Map<String, Map<String, Integer>>> marks) {
		this.marks = marks;
	}


    
}
