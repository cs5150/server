package edu.cornell.svms.model.onload;


import java.io.Serializable;

/**
 * Created by apple on 3/3/18.
 */

public class Teacher implements Serializable {

    private String name;
    private String contact;
    private String address;
    private Standard classTeacher;
    private String id;
    private String schoolAddress;
    private String schoolContact;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Standard getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(Standard classTeacher) {
        this.classTeacher = classTeacher;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getSchoolContact() {
        return schoolContact;
    }

    public void setSchoolContact(String schoolContact) {
        this.schoolContact = schoolContact;
    }
    

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Teacher [name=");
		builder.append(name);
		builder.append(", contact=");
		builder.append(contact);
		builder.append(", address=");
		builder.append(address);
		builder.append(", classTeacher=");
		builder.append(classTeacher);
		builder.append(", id=");
		builder.append(id);
		builder.append(", schoolAddress=");
		builder.append(schoolAddress);
		builder.append(", schoolContact=");
		builder.append(schoolContact);
		builder.append("]");
		return builder.toString();
	}  
}
