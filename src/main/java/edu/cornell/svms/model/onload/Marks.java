package edu.cornell.svms.model.onload;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by apple on 3/24/18.
 */

public class Marks implements Serializable {
    private Map<String, Integer> marks;

    public Map<String, Integer> getMarks() {
        return marks;
    }

    public void setMarks(Map<String, Integer> marks) {
        this.marks = marks;
    }
}
