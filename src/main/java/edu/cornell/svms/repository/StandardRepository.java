package edu.cornell.svms.repository;

import org.springframework.data.repository.CrudRepository;

import edu.cornell.svms.model.Standard;

public interface StandardRepository extends CrudRepository<Standard, Long> {

	// @Query("SELECT creation_date FROM Votes WHERE post_id= ?1 AND vote_type_id=
	// 2")
	// List<Date> findUpVotesByPostId(Integer postId);
	//
	// @Query("SELECT creation_date FROM Votes WHERE post_id= ?1 AND vote_type_id=
	// 3")
	// List<Date> findDownVotesByPostId(Integer postId);
}
