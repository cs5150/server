package edu.cornell.svms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.cornell.svms.enumeration.Subject;
import edu.cornell.svms.model.Course;

public interface CourseRepository extends CrudRepository<Course, Long> {

	Course findById(Long id);
	
	Course findBySubjectName(Subject subject);
	
	List<Course> findByStandardId(Long standardId);
	
	// @Query("SELECT creation_date FROM Votes WHERE post_id= ?1 AND vote_type_id=
	// 2")
	// List<Date> findUpVotesByPostId(Integer postId);
	//
	// @Query("SELECT creation_date FROM Votes WHERE post_id= ?1 AND vote_type_id=
	// 3")
	// List<Date> findDownVotesByPostId(Integer postId);
}
