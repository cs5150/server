package edu.cornell.svms.repository;

import org.springframework.data.repository.CrudRepository;

import edu.cornell.svms.model.AbsentRecord;

public interface AbsentRecordRepository extends CrudRepository<AbsentRecord, Long> {

	AbsentRecord findById(Long id);
	
}
