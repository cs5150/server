package edu.cornell.svms.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.cornell.svms.model.Homework;

public interface HomeworkRepository extends CrudRepository<Homework, Long> {

	Homework findById(Long id);
	
	Homework findByDescription(String description);
	
	List<Homework> findByCourseId(Long courseId);

//	List<Homework> findByCourseIds(Long courseIds);
}

