package edu.cornell.svms.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.cornell.svms.model.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {
	 @Query("SELECT id FROM Student WHERE standard_id= ?1")
	 Set<Long> findStudentsByStandard(Long standard);
}
