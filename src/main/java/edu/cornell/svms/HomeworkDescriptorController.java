//package edu.cornell.svms;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import edu.cornell.svms.model.HomeworkDescriptor;
//import edu.cornell.svms.repository.HomeworkDescriptorRepository;
//
//@Controller
//@RequestMapping(path = "/homeworkdescriptor")
//public class HomeworkDescriptorController {
//
//	@Autowired
//	HomeworkDescriptorRepository homeworkDescriptorRepo;
//
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public List<HomeworkDescriptor> getAllHomeworks() {
//		List<HomeworkDescriptor> allStudents = new ArrayList<HomeworkDescriptor>();
//		allStudents.addAll((List<HomeworkDescriptor>) homeworkDescriptorRepo.findAll());
//		return allStudents;
//	}
//	
//	@RequestMapping(value = { "/{id}/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public HomeworkDescriptor getHomeworkById(@PathVariable("id") Long id) {
//		return homeworkDescriptorRepo.findOne(id);
//	}
//
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public List<HomeworkDescriptor> addHomeworks(@RequestBody List<HomeworkDescriptor> homework) {
//		homeworkDescriptorRepo.save(homework);
//		return homework;
//	}
//	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllHomeworks(@RequestBody List<HomeworkDescriptor> homework) {
//		homeworkDescriptorRepo.delete(homework);
//	}
//
//	@RequestMapping(value = { "/{id}/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteHomeworkById(@PathVariable("id") Long id) {
//		homeworkDescriptorRepo.delete(id);
//	}
//
//}
