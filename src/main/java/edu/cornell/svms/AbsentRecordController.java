package edu.cornell.svms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.cornell.svms.model.AbsentRecord;
import edu.cornell.svms.repository.AbsentRecordRepository;

@Controller
@RequestMapping(path = "/absent")
public class AbsentRecordController {

	@Autowired
	AbsentRecordRepository absentRecRepo;

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<AbsentRecord> getAllStudents() {
		List<AbsentRecord> allStudents = new ArrayList<AbsentRecord>();
		allStudents.addAll((List<AbsentRecord>) absentRecRepo.findAll());
		return allStudents;
	}
	
	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<AbsentRecord> postStudents(@RequestBody List<AbsentRecord> student_id) {
		absentRecRepo.save(student_id);
		return student_id;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public AbsentRecord getStudentById(@PathVariable("id") Long id) {
		return absentRecRepo.findOne(id);
	}
	
	
	
	

}
