package edu.cornell.svms;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.cornell.svms.model.AllStudentInfo;
import edu.cornell.svms.model.Standard;
import edu.cornell.svms.model.Student;
import edu.cornell.svms.repository.StandardRepository;
import edu.cornell.svms.repository.StudentRepository;

@Controller
@RequestMapping(path = "/app")
public class OnAppLoadController {

	@Autowired
	StudentRepository studentRepo;
	
	@Autowired
	StandardRepository standardRepo;

	@RequestMapping(value = { "/student/{admissionno}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public AllStudentInfo getAllStudents(@PathVariable("admissionno") Long admissionNo) {
		// get studentInfo, standard, courses, teacher, last 10 days HW. 
		Student student = studentRepo.findOne(admissionNo);
		Standard standard = standardRepo.findOne(student.getStandardId());
		standard.setStudentsIds(null);
		Set<Long> courses = standard.getCoursesIds();
		// find HW by course Id and last 10 days 
		
		AllStudentInfo allStudentInfo = new AllStudentInfo();
		allStudentInfo.setStudentInfo(studentRepo.findOne(admissionNo));
		
		return allStudentInfo;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public Student getStudentById(@PathVariable("id") Long id) {
		return studentRepo.findOne(id);
	}

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Student> postStudents(@RequestBody List<Student> students) {
		studentRepo.save(students);
		return students;
	}
	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllStudents(@RequestBody List<Student> students) {
//		studentRepo.delete(students);
//	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void deleteStudentById(@PathVariable("id") Long id) {
		studentRepo.delete(id);
	}

}
