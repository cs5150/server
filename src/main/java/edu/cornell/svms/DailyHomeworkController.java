//package edu.cornell.svms;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import edu.cornell.svms.model.DailyHomework;
//import edu.cornell.svms.repository.DailyHomeworkRepository;
//
//@Controller
//@RequestMapping(path = "/dailyhomework")
//public class DailyHomeworkController {
//
//	@Autowired
//	DailyHomeworkRepository homeworkRepo;
//
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public List<DailyHomework> getAllDailyHomeworks() {
//		List<DailyHomework> allStudents = new ArrayList<DailyHomework>();
//		allStudents.addAll((List<DailyHomework>) homeworkRepo.findAll());
//		return allStudents;
//	}
//	
//	@RequestMapping(value = { "/id/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public DailyHomework getDailyHomeworkById(@PathVariable("id") Long id) {
//		return homeworkRepo.findOne(id);
//	}
//
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public List<DailyHomework> addDailyHomeworks(@RequestBody List<DailyHomework> homework) {
//		homeworkRepo.save(homework);
//		return homework;
//	}
//	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllDailyHomeworks(@RequestBody List<DailyHomework> homework) {
//		homeworkRepo.delete(homework);
//	}
//	
//	@RequestMapping(value = { "/id/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteDailyHomeworkById(@PathVariable("id") Long id) {
//		homeworkRepo.delete(id);
//	}
//
//}
