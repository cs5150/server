package edu.cornell.svms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.cornell.svms.model.Course;
import edu.cornell.svms.model.ErrorMessage;
import edu.cornell.svms.model.Teacher;
import edu.cornell.svms.repository.CourseRepository;
import edu.cornell.svms.repository.TeacherRepository;

@Controller
@RequestMapping(path = "/course")
@JsonInclude(Include.NON_NULL)
public class CourseController {

	@Autowired
	CourseRepository courseRepo;
	
	@Autowired
	TeacherRepository teacherRepo;

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Course> getAllCourses() {
		List<Course> allStudents = new ArrayList<Course>();
		allStudents.addAll((List<Course>) courseRepo.findAll());
		return allStudents;
	}
	
	
	@RequestMapping(value = { "/{id}" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public Course getCoursesById(@PathVariable("id") Long id) {
		return courseRepo.findOne(id);
	}

	
	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Course> addCourses(@RequestBody List<Course> course) {
		courseRepo.save(course);
		return course;
	}
	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllCourses(@RequestBody List<Course> course) {
//		courseRepo.delete(course);
//	}
	
	@RequestMapping(value = { "/{id}" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void deleteCoursesById(@PathVariable("id") Long id) {
		courseRepo.delete(id);
	}
	
	@RequestMapping(value = { "/{courseid}/{teacherid}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.PUT }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public ErrorMessage mapTeacherToCourse(@PathVariable("courseid") Long courseId,@PathVariable("teacherid") Long teacherId) {
		Course c = courseRepo.findOne(courseId);
		if(c== null)
			return ErrorMessage.INVALID_COURSE;
		Teacher t = teacherRepo.findOne(teacherId);
		if(t== null)
			return ErrorMessage.INVALID_TEACHER;
		c.setTeacherId(teacherId);
		courseRepo.save(c);
		return ErrorMessage.SUCCESS;
	}
}
