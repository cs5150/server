//package edu.cornell.svms;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import edu.cornell.svms.model.Standard;
//import edu.cornell.svms.repository.StandardInfoRepository;
//
//@Controller
//@RequestMapping(path = "/standardinfo")
//public class StandardInfoController {
//
//	@Autowired
//	StandardInfoRepository standardInfoRepo;
//
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public List<Standard> getAllStandardInfos() {
//		List<Standard> allStudents = new ArrayList<Standard>();
//		allStudents.addAll((List<Standard>) standardInfoRepo.findAll());
//		return allStudents;
//	}
//	
//	@RequestMapping(value = { "/{id}/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public Standard getStandardInfoById(@PathVariable("id") Long id) {
//		return standardInfoRepo.findOne(id);
//	}
//
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public List<Standard> addStandardInfos(@RequestBody List<Standard> standardInfo) {
//		standardInfoRepo.save(standardInfo);
//		return standardInfo;
//	}
//	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllStandardInfos(@RequestBody List<Standard> standardInfo) {
//		standardInfoRepo.delete(standardInfo);
//	}
//	
//	@RequestMapping(value = { "/{id}/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteStandardInfoById(@PathVariable("id") Long id) {
//		standardInfoRepo.delete(id);
//	}
//
//}
