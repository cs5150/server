package edu.cornell.svms;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.cornell.svms.model.Standard;
import edu.cornell.svms.repository.StandardRepository;
import edu.cornell.svms.repository.StudentRepository;

@Controller
@RequestMapping(path = "/standard")
public class StandardController {

	@Autowired
	StandardRepository standardRepo;
	
	@Autowired
	StudentRepository studentRepo;

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Standard> getAllStandards() {
		List<Standard> allStudents = new ArrayList<Standard>();
		allStudents.addAll((List<Standard>) standardRepo.findAll());
		return allStudents;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public Standard getStandardById(@PathVariable("id") Long id) {
		return standardRepo.findOne(id);
	}

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Standard> addStandards(@RequestBody List<Standard> standard) {
		standardRepo.save(standard);
		return standard;
	}
	
	@RequestMapping(value = { "students/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Standard> addStudentsToStandards() {
		List<Standard> standards= (ArrayList<Standard>) standardRepo.findAll();
		for(Standard standard: standards) {
			Set<Long> studentsIds = studentRepo.findStudentsByStandard(standard.getId());
			standard.setStudentsIds(studentsIds);
		}
		standardRepo.save(standards);
		return standards;
	}
	
	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllStandards(@RequestBody List<Standard> standard) {
//		standardRepo.delete(standard);
//	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void deleteStandardsById(@PathVariable("id") Long id) {
		standardRepo.delete(id);
	}

}
