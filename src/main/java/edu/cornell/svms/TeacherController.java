package edu.cornell.svms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.cornell.svms.model.Teacher;
import edu.cornell.svms.repository.TeacherRepository;

@Controller
@RequestMapping(path = "/teacher")
@JsonInclude(Include.NON_NULL)
public class TeacherController {

	@Autowired
	TeacherRepository teacherRepo;

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Teacher> getAllTeachers() {
		List<Teacher> allTeachers = new ArrayList<Teacher>();
		allTeachers.addAll((List<Teacher>) teacherRepo.findAll());
		return allTeachers;
	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public Teacher getTeacherById(@PathVariable("id") Long id) {
		return teacherRepo.findOne(id);
	}

	@RequestMapping(value = { "/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public List<Teacher> addTeachers(@RequestBody List<Teacher> teachers) {
		teacherRepo.save(teachers);
		return teachers;
	}
	
//	@RequestMapping(value = { "/" }, method = {
//			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
//					"application/json; charset=utf-8" })
//	@ResponseBody
//	public void deleteAllTeachers() {
//		teacherRepo.deleteAll();
//	}
	
	@RequestMapping(value = { "/{id}/" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.DELETE }, produces = {
					"application/json; charset=utf-8" })
	@ResponseBody
	public void deleteTeacherById(@PathVariable("id") Long id) {
		teacherRepo.delete(id);
	}

}
